using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace LTAF
{
    /// <summary>
    /// Represents the style information of an html element.
    /// </summary>
    /// <remarks>
    /// http://www.w3.org/TR/REC-CSS2/propidx.html
    /// </remarks>
    public class HtmlStyle
    {
        private string _style;
        private HtmlAttributeDictionary _descriptors;

        public HtmlStyle()
        {
            _descriptors = new HtmlAttributeDictionary();
        }

        public void LoadDescriptors(string style)
        {
            _style = style;
            string[] descriptors = _style.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string desc in descriptors)
            {
                string[] keyValue = desc.Split(':');
                if (keyValue.Length == 2)
                {
                    _descriptors.Add(keyValue[0].Trim().Replace("-", "").ToLowerInvariant(), keyValue[1].Trim().ToLowerInvariant());
                }
            }
        }

        #region Get<T>
        /// <summary>
        /// Get the value of a style descriptor
        /// </summary>
        /// <typeparam name="T">Type of the style descriptor</typeparam>
        /// <param name="key">Style descriptor name</param>
        /// <returns>Value of the style descriptor</returns>
        public T Get<T>(string key)
        {
            return _descriptors.Get<T>(key);
        }

        /// <summary>
        /// Get the value of a style descriptor
        /// </summary>
        /// <typeparam name="T">Type of the style descriptor</typeparam>
        /// <param name="key">Style descriptor name</param>
        /// <param name="defaultValue">Default value if attribute not found</param>
        /// <returns>Value of the style descriptor</returns>
        public T Get<T>(string key, T defaultValue)
        {
            return _descriptors.Get<T>(key, defaultValue);
        }
        #endregion

        /// <summary>
        /// The css style as a string
        /// </summary>
        public string RawStyle
        {
            get
            {
                return _style;
            }
        }

        internal IDictionary<string, string> Descriptors
        {
            get { return _descriptors; }
        }

        /// <summary>
        /// Visibility descriptor
        /// </summary>
        public Visibility Visibility
        {
            get
            {
                return _descriptors.Get<Visibility>("visibility", Visibility.NotSet);
            }
        }

        /// <summary>
        /// Position descriptor
        /// </summary>
        public Position Position
        {
            get
            {
                return _descriptors.Get<Position>("position", Position.NotSet);
            }
        }

        /// <summary>
        /// Background descriptor
        /// </summary>
        public System.Drawing.Color BackgroundColor
        {
            get
            {
                return _descriptors.Get<Color>("backgroundcolor", Color.Empty);
            }
        }

        /// <summary>
        /// BackgroundImage descriptor
        /// </summary>
        public string BackgroundImage
        {
            get
            {
                return _descriptors.Get<string>("backgroundimage", null);
            }
        }

        /// <summary>
        /// Color descriptor
        /// </summary>
        public System.Drawing.Color Color
        {
            get
            {
                return _descriptors.Get<Color>("color", Color.Empty);
            }
        }

        /// <summary>
        /// WhiteSpace descriptor
        /// </summary>
        public WhiteSpace WhiteSpace
        {
            get
            {
                return _descriptors.Get<WhiteSpace>("whitespace", WhiteSpace.NotSet);
            }
        }

        /// <summary>
        /// Display descriptor
        /// </summary>
        public Display Display
        {
            get
            {
                return _descriptors.Get<Display>("display", Display.NotSet);
            }
        }

        /// <summary>
        /// Overflow descriptor
        /// </summary>
        public Overflow Overflow
        {
            get
            {
                return _descriptors.Get<Overflow>("overflow", Overflow.NotSet);
            }
        }
    }
}
