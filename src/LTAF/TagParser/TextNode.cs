//------------------------------------------------------------------------------
// <copyright file="TextNode.cs" author="CarlosAg">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace LTAF.TagParser
{

    public class TextNode : Node
    {

        public TextNode()
        {
        }

        public override NodeType Type
        {
            get
            {
                return NodeType.Text;
            }
        }

    }
}
