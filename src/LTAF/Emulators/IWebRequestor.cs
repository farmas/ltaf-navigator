﻿using System;
using System.Net;

namespace LTAF.Emulators
{
    public interface IWebRequestor
    {
        HttpWebRequest CreateRequest(string url);
        HttpWebResponse ExecuteRequest(HttpWebRequest request);
    }
}
